# Dotfiles
My dotfiles based on the Catppuccin color pallete

## Including
- Neovim
- Firefox

## Screenshots
<p align = center>Neovim</p>
<img src = "https://github.com/tpncoder/dotfiles/blob/main/WindowsTerminal_zkE3foMSPE.png">
<br>
<img src = "https://github.com/tpncoder/dotfiles/blob/main/WindowsTerminal_29dVubZ5rD.png">
<br>
<img src = "https://github.com/tpncoder/dotfiles/blob/main/WindowsTerminal_0FSh1X7Cs9.png">
<br>

<p align = center>Firefox</p>
<img src = "https://github.com/tpncoder/dotfiles/blob/main/firefox_WzNqNd7CXJ.png">
