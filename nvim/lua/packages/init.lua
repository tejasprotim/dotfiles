	--packages
require "paq" {
	  	"savq/paq-nvim",
		'wbthomason/packer.nvim',
	
		--some shit
	  	"nvim-telescope/telescope.nvim",
	  	--"nvim-neo-tree/neo-tree.nvim",
	  	"nvim-treesitter/nvim-treesitter",
	 	"kyazdani42/nvim-tree.lua",
	 	"kyazdani42/nvim-web-devicons",
	  	"nvim-lua/plenary.nvim",
	  	"folke/which-key.nvim",
	
		--colorschemes
	  	"catppuccin/nvim",
	  	"arcticicestudio/nord-vim",
 	  	"dracula/vim",
	  	"folke/tokyonight.nvim",
		'navarasu/onedark.nvim',
		"ellisonleao/gruvbox.nvim",
		'NTBBloodbath/doom-one.nvim',
		'tomasiser/vim-code-dark',

		--completion and lsp
	  	"hrsh7th/vim-vsnip",
	  	"hrsh7th/vim-vsnip",
	  	"hrsh7th/vim-vsnip-integ",
	  	"neovim/nvim-lspconfig",
	  	"hrsh7th/cmp-nvim-lsp",
	  	"hrsh7th/cmp-buffer",
	  	"hrsh7th/cmp-path",
	  	"hrsh7th/cmp-cmdline",	 
	  	"hrsh7th/nvim-cmp",
	  	"L3MON4D3/LuaSnip",
	  	"saadparwaiz1/cmp_luasnip",
	  	"williamboman/nvim-lsp-installer",

		--icing of the cake
	 	"rcarriga/nvim-notify",
		"nvim-lualine/lualine.nvim",
	  	'folke/which-key.nvim', 
	 	'akinsho/bufferline.nvim',
		--"glepnir/dashboard-nvim",
	  	"karb94/neoscroll.nvim",
		'feline-nvim/feline.nvim',
	  	--'itchyny/lightline.vim',
	  	'goolord/alpha-nvim',
	  	"akinsho/toggleterm.nvim",
		"noib3/nvim-cokeline",

	  	"lervag/vimtex",
	  	"mattn/efm-langserver",
	  	"pangloss/vim-javascript",
	  	"MaxMEllon/vim-jsx-pretty",
	  	"airblade/vim-gitgutter",
	  	"tpope/vim-unimpaired",
	  	"tpope/vim-surround",
		'ellisonleao/glow.nvim',
		"mhartington/formatter.nvim",
  	  	"alvan/vim-closetag",
	  	"jiangmiao/auto-pairs",
		'Pocco81/TrueZen.nvim',
			}
